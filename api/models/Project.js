const moment = require("moment");
module.exports = {
  attributes: {
    name: { type: "string", required: true },
    description: { type: "string", required: true },
    startDate: { type: "ref", columnType: "datetime", defaultsTo: null },
    endDate: { type: "ref", columnType: "datetime", defaultsTo: null },
    status: { type: "string", isIn: ["open", "close"], defaultsTo: "open" },
    employees: {
      type: "json",
      columnType: "array",
      defaultsTo: [],
      example: [
        {
          employee: { model: "user", required: true },
        },
      ],
    },
    owner: { model: "user" },
    tasks: { collection: "task", via: "project" },
  },
  beforeCreate: async (values, proceed) => {
    try {
      transformAndValidateValues(values);
    } catch (error) {
      return proceed(error);
    }
    return proceed();
  },
  beforeUpdate: async (values, proceed) => {
    try {
      transformAndValidateValues(values);
    } catch (error) {
      return proceed(error);
    }
    return proceed();
  },
};

const transformAndValidateValues = (values) => {
  const { startDate, endDate } = values;
  if (startDate) {
    values.startDate = moment(startDate, "MM-DD-YYYY").toDate();
  }
  if (endDate) {
    values.endDate = moment(endDate, "MM-DD-YYYY").toDate();
  }
  if (
    values.startDate &&
    values.endDate &&
    !moment(values.startDate).isBefore(values.endDate)
  ) {
    throw new Error("startDate must be before endDate");
  }
};
