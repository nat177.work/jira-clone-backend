const bcrypt = require("bcrypt");

module.exports = {
  attributes: {
    username: { type: "string", required: true, unique: true },
    password: { type: "string", allowNull: true },
    email: { type: "string", required: true, unique: true },
    fullname: { type: "string", required: true },
    role: { type: "string", isIn: ["admin", "user"], defaultsTo: "user" },
    tasks: { collection: "task", via: "assignee" },
    projects: { collection: "project", via: "owner" },
    provider: {
      type: "string",
      isIn: ["local", "google", "outlook"],
      defaultsTo: "local",
    },
  },
  customToJSON: function () {
    delete this.password;
    return this;
  },
  beforeCreate: async (values, proceed) => {
    const { password } = values;
    if (password) {
      const hashedPassword = await bcrypt.hash(password, 10);
      values.password = hashedPassword;
    }
    return proceed();
  },
};
