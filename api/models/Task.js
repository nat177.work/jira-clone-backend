const moment = require("moment");
module.exports = {
  attributes: {
    title: { type: "string", required: true },
    description: { type: "string", required: true },
    dueDate: { type: "ref", columnType: "datetime", defaultsTo: null },
    status: {
      type: "string",
      isIn: ["to-do", "in-progress", "done", "review", "testing"],
      defaultsTo: "to-do",
    },
    priority: {
      type: "string",
      isIn: ["low", "medium", "high"],
      defaultsTo: "low",
    },
    reviewers: {
      type: "json",
      columnType: "array",
      defaultsTo: [],
      example: [
        {
          reviewer: { model: "user", required: true },
        },
      ],
    },
    assignee: { model: "user" },
    project: { model: "project" },
  },
  beforeCreate: async (values, proceed) => {
    const { dueDate } = values;
    if (dueDate) {
      values.dueDate = moment(dueDate, "MM-DD-YYYY").toDate();
    }
    return proceed();
  },
  beforeUpdate: async (values, proceed) => {
    const { dueDate } = values;
    if (dueDate) {
      values.dueDate = moment(dueDate, "MM-DD-YYYY").toDate();
    }
    return proceed();
  },
};
