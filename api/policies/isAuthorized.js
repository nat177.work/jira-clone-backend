const jwt = require("jsonwebtoken");
module.exports = async (req, res, proceed) => {
  if (!req.headers) {
    return res.status(401).send({ message: "headers is required" });
  }
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).send({ message: "authorization is required" });
  }
  const [type, token] = authorization.split(" ");
  if (type !== "Bearer") {
    return res.status(401).send({ message: "authorization is invalid" });
  }
  if (!token) {
    return res.status(401).send({ message: "token is required" });
  }
  try {
    const payload = await jwt.verify(token, process.env.JWT_SECRET);
    req.user = payload;
  } catch (error) {
    return res.status(401).send({ message: error.message });
  }
  return proceed();
};
