module.exports = async function (req, res, proceed) {
  const projectId = req.params.id;
  const project = await Project.findOne({ id: projectId });
  if (!project) {
    return res
      .status(404)
      .send({ message: `Project with id ${projectId} not found` });
  }
  if (project.owner !== req.user.id) {
    return res
      .status(403)
      .send({ message: "You are not the owner of this project" });
  }
  return proceed();
};
