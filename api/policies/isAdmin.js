module.exports = (req, res, proceed) => {
  const { user } = req;
  if (!user) {
    return res.unauthorized({ message: "you are not authorized" });
  }
  if (user.role !== "admin") {
    return res.forbidden({ message: "you are not admin" });
  }
  return proceed();
};
