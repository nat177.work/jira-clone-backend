const moment = require("moment");
const TasksService = require("../services/TasksService");
module.exports = {
  find: async (req, res) => {
    try {
      const tasks = await TasksService.find(req.query);
      return res.ok(tasks);
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
  create: async (req, res) => {
    const {
      title,
      description,
      dueDate,
      status,
      priority,
      reviewers,
      assignee,
      project,
    } = req.body;
    if (!title) {
      return res.badRequest({ message: "title is required" });
    }
    if (!description) {
      return res.badRequest({ message: "description is required" });
    }
    if (dueDate && !moment(new Date(dueDate)).isValid()) {
      return res.badRequest({ message: "dueDate is not valid" });
    }
    try {
      const task = await TasksService.create({
        title,
        description,
        dueDate,
        status,
        priority,
        reviewers,
        assignee,
        project,
      });
      return res.ok(task);
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
  findOne: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      return res.badRequest({ message: "id is required" });
    }
    try {
      const task = await TasksService.findOne(id);
      if (!task) {
        return res
          .status(404)
          .send({ message: `Task with id ${id} not exists` });
      }
      return res.ok(task);
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
  update: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      return res.badRequest({ message: "id is required" });
    }
    const { dueDate } = req.body;

    if (dueDate && !moment(new Date(dueDate)).isValid()) {
      return res.badRequest({ message: "dueDate is not valid" });
    }
    try {
      const task = await TasksService.update(id, req.body);
      res.ok(task);
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
  delete: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      return res.badRequest({ message: "id is required" });
    }
    try {
      const project = await TasksService.remove(id);
      if (!project) {
        res.status(404).send({ message: `Task with id ${id} not exists` });
      }
      return res.ok({ message: `Delete task with id ${id} successfully` });
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
};
