/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const { passport } = require("../services/PassportService");
const AuthService = require("../services/AuthService");
module.exports = {
  login: async (req, res) => {
    try {
      const { username, password } = req.body;
      const data = await AuthService.login(username, password);
      res.ok(data);
    } catch (error) {
      res.badRequest({ message: error.message });
    }
  },
  logout: async (req, res) => {
    req.logout((err) => {
      if (err) {
        return res.serverError({ message: err.message });
      }
      return res.ok({ message: "Logout successfully!" });
    });
  },
  register: async (req, res) => {
    const { username, email, fullname } = req.body;
    if (!username) {
      return res.badRequest({ message: "username is required" });
    }
    if (!email) {
      return res.badRequest({ message: "email is required" });
    }
    if (!fullname) {
      return res.badRequest({ message: "fullname is required" });
    }
    try {
      await AuthService.register({ username, email, fullname });
      return res.ok({
        message: "Register successfully. Check your email to get password!",
      });
    } catch (err) {
      return res.serverError({ message: err.message });
    }
  },
  google: async (req, res) => {
    passport.authenticate("google", {
      scope: ["profile", "email"],
    })(req, res);
  },
  googleCallback: async (req, res) => {
    passport.authenticate("google", (err, user, info) => {
      if (err) {
        return res.serverError({ message: err.message });
      }
      if (!user) {
        return res.badRequest({ message: info && info.message });
      }
      req.login(user, (err) => {
        if (err) {
          return res.serverError({ message: err.message });
        }
        const tokens = sails.helpers.generateToken(user);
        return res.ok(tokens);
      });
    })(req, res);
  },
  outlook: async (req, res) => {
    if (req.user) {
      return res.ok(req.user);
    }
    passport.authenticate("windowslive", {
      scope: [
        "openid",
        "profile",
        "offline_access",
        "https://outlook.office.com/Mail.Read",
      ],
    })(req, res);
  },
  outlookCallback: async (req, res) => {
    passport.authenticate("windowslive", (err, user, info) => {
      if (err) {
        return res.serverError({ message: err.message });
      }
      if (!user) {
        return res.badRequest({ message: info && info.message });
      }
      req.login(user, (err) => {
        if (err) {
          return res.serverError({ message: err.message });
        }
        const tokens = sails.helpers.generateToken(user);
        return res.ok(tokens);
      });
    })(req, res);
  },
};
