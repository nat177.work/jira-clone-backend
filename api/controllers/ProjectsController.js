const moment = require("moment");
const ProjectsService = require("../services/ProjectsService");
module.exports = {
  find: async (req, res) => {
    try {
      const projects = await ProjectsService.find(req.query);
      return res.ok(projects);
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
  create: async (req, res) => {
    const { name, description, startDate, endDate, employees } = req.body;
    if (!name) {
      return res.badRequest({ message: "name is required" });
    }
    if (!description) {
      return res.badRequest({ message: "description is required" });
    }
    if (startDate && !moment(new Date(startDate)).isValid()) {
      return res.badRequest({ message: "startDate is not valid" });
    }
    if (endDate && !moment(new Date(endDate)).isValid()) {
      return res.badRequest({ message: "endDate is not valid" });
    }
    try {
      const project = await ProjectsService.create({
        name,
        description,
        startDate,
        endDate,
        employees,
        owner: req.user.id,
      });
      return res.ok(project);
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
  findOne: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      return res.badRequest({ message: "id is required" });
    }
    try {
      const project = await ProjectsService.findOne(id);
      if (!project) {
        return res
          .status(404)
          .send({ message: `Project with id ${id} not exists` });
      }
      return res.ok(project);
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
  update: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      return res.badRequest({ message: "id is required" });
    }
    const { startDate, endDate } = req.body;

    if (startDate && !moment(new Date(startDate)).isValid()) {
      return res.badRequest({ message: "startDate is not valid" });
    }
    if (endDate && !moment(new Date(endDate)).isValid()) {
      return res.badRequest({ message: "endDate is not valid" });
    }
    try {
      const project = await ProjectsService.update(id, req.body);
      res.ok(project);
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
  delete: async (req, res) => {
    const { id } = req.params;
    if (!id) {
      return res.badRequest({ message: "id is required" });
    }
    try {
      const project = await ProjectsService.remove(id);
      if (!project) {
        res.status(404).send({ message: `Project with id ${id} not exists` });
      }
      return res.ok({ message: `Delete project with id ${id} successfully` });
    } catch (err) {
      return res.badRequest({ message: err.message });
    }
  },
};
