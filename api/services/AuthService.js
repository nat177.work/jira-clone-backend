const crypto = require("crypto");
const MailService = require("./MailService");
const UsersService = require("./UsersService");
const jwt = require("jsonwebtoken");
module.exports = {
  register: async (userInfo) => {
    const password = crypto.randomBytes(12).toString("hex");
    const mailOptions = {
      from: "test@email.com",
      to: userInfo.email,
      subject: "Welcome to our site",
      text: `Your password is ${password}`,
      html: `<p>Your password is <b>${password}</b>. 
        Please change the password the first time you log in!</p>`,
    };
    try {
      await UsersService.create({ ...userInfo, password });
      await MailService.sendMail(mailOptions);
    } catch (error) {
      throw error;
    }
  },
  login: async (username, password) => {
    const user = await sails.helpers.validateUser(username, password);
    if (!user) {
      throw new Error("Invalid username or password");
    }
    const tokens = await sails.helpers.generateTokens(user);
    return tokens;
  },
  refresh: (user) => {
    const tokens = sails.helpers.generateToken(user);
    return tokens;
  },
};
