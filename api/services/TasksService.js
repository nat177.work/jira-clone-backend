module.exports = {
  create: async (task) => {
    const newTask = await Task.create(task).fetch();
    return newTask;
  },
  find: async (query) => {
    const { limit, skip, assinee, status, priority, keyword } = query;
    const tasksQuery = Task.find();
    if (limit) {
      tasksQuery.limit(limit);
    }
    if (skip) {
      tasksQuery.skip(skip);
    }
    if (assinee) {
      tasksQuery.where({ assinee });
    }
    if (status) {
      tasksQuery.where({ status });
    }
    if (priority) {
      tasksQuery.where({ priority });
    }
    if (keyword) {
      tasksQuery.where({ title: { contains: keyword } });
    }
    const tasks = await tasksQuery;
    return tasks;
  },
  findOne: async (id) => {
    const task = Task.findOne({ id });
    return task;
  },
  update: async (id, task) => {
    const foundTask = await Task.findOne({ id });
    if (!foundTask) {
      throw new Error("Task not found!");
    }
    const updatedTask = await Task.update(
      { id },
      Object.assign(foundTask, task)
    ).fetch();
    return updatedTask;
  },
  remove: async (id) => {
    const task = await Task.destroyOne({ id });
    return task;
  },
};
