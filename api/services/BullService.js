const Bull = require("bull");
const opts = {
  redis: {
    port: process.env.REDIS_PORT || 6379,
    host: process.env.REDIS_HOST || "localhost",
  },
};

const initQueue = (queueName) => {
  return new Bull(queueName, opts);
};

module.exports = { initQueue };
