module.exports = {
  create: async (user) => {
    const newUser = await User.create(user).fetch();
    return newUser;
  },
  find: async (criteria) => {
    const users = await User.find(criteria);
    return users;
  },
  findOne: async (criteria) => {
    const user = await User.findOne(criteria);
    return user;
  },
  update: async (id, user) => {
    const updatedUser = await User.updateOne({ id }).set(user);
    return updatedUser;
  },
  delete: async (id) => {
    const deletedUser = await User.destroyOne({ id });
    return deletedUser;
  },
};
