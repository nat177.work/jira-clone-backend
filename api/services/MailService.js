const { initQueue } = require("./BullService");

const sendMailQueue = initQueue("sendMail");
module.exports = {
  sendMail: (mailOptions) => {
    return sendMailQueue.add({ mailOptions });
  },
};
