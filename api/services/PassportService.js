const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const GoogleStrategy = require("passport-google-oauth2").Strategy;
const OutlookStrategy = require("passport-outlook").Strategy;
const bcrypt = require("bcrypt");
const {
  GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET,
  OUTLOOK_CLIENT_ID,
  OUTLOOK_CLIENT_SECRET,
} = process.env;

passport.use(
  new LocalStrategy((username, password, done) => {
    User.findOne({ username: username })
      .then((user) => {
        if (!user) {
          return done(null, false, { message: "Incorrect username" });
        }
        bcrypt.compare(password, user.password, (err, res) => {
          if (err) {
            return done(err);
          }
          if (res) {
            delete user.password;
            return done(null, user);
          }
          return done(null, false, { message: "Incorrect password" });
        });
      })
      .catch((err) => done(err));
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: "http://localhost:1337/auth/google/callback",
      passReqToCallback: true,
    },
    async (request, accessToken, refreshToken, profile, done) => {
      try {
        const user = await User.findOne({
          email: profile.email,
          provider: "google",
        });
        if (user) {
          return done(null, user);
        }
        const newUser = await User.create({
          username: profile.email,
          email: profile.email,
          fullname: profile.displayName,
          provider: "google",
        }).fetch();
        return done(null, newUser);
      } catch (error) {
        return done(error);
      }
    }
  )
);

passport.use(
  new OutlookStrategy(
    {
      clientID: OUTLOOK_CLIENT_ID,
      clientSecret: OUTLOOK_CLIENT_SECRET,
      callbackURL: "http://localhost:1337/auth/outlook/callback",
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const user = await User.findOne({ email: profile._json.EmailAddress });
        if (user) {
          return done(null, user);
        }
        const newUser = await User.create({
          username: profile._json.EmailAddress,
          email: profile._json.EmailAddress,
          fullname: profile._json.DisplayName,
          provider: "outlook",
          providerId: profile._json.Id,
        }).fetch();
        return done(null, newUser);
      } catch (error) {
        return done(error);
      }
    }
  )
);

passport.serializeUser((user, done) => done(null, user.id));

passport.deserializeUser((id, done) => {
  User.findOne({ id })
    .then((user) => {
      delete user.password;
      done(null, user);
    })
    .catch((err) => done(err));
});

module.exports = { passport };
