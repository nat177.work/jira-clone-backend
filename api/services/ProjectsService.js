module.exports = {
  create: async (project) => {
    const newProject = await Project.create(project).fetch();
    return newProject;
  },
  find: async (query) => {
    const { limit, skip, owner, status, keyword } = query;
    const projectsQuery = Project.find();
    if (limit) {
      projectsQuery.limit(limit);
    }
    if (skip) {
      projectsQuery.skip(skip);
    }
    if (owner) {
      projectsQuery.where({ owner });
    }
    if (status) {
      projectsQuery.where({ status });
    }
    if (keyword) {
      projectsQuery.where({ name: { contains: keyword } });
    }
    const projects = await projectsQuery;
    return projects;
  },
  findOne: async (id) => {
    const project = await Project.findOne({ id });
    return project;
  },
  update: async (id, project) => {
    const foundProject = await Project.findOne({ id });
    if (!foundProject) {
      throw new Error("Project not found!");
    }
    const updatedProject = await Project.update(
      { id },
      Object.assign(foundProject, project)
    ).fetch();
    return updatedProject;
  },
  remove: async (id) => {
    const project = await Project.destroyOne({ id });
    return project;
  },
};
