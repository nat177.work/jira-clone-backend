const jwt = require("jsonwebtoken");
module.exports = {
  friendlyName: "Generate tokens",

  description: "Generate tokens from user and secret keys",

  inputs: {
    user: {
      type: "ref",
      required: true,
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: (inputs, exits) => {
    const { user } = inputs;
    const payload = {
      id: user.id,
      username: user.username,
      role: user.role,
    };
    const tokens = {
      access_token: jwt.sign(payload, process.env.JWT_SECRET, {
        expiresIn: "1h",
      }),
      refresh_token: jwt.sign(payload, process.env.JWT_SECRET, {
        expiresIn: "7d",
      }),
    };
    exits.success(tokens);
  },
};
