const bcrypt = require("bcrypt");
module.exports = {
  friendlyName: "Validate user",

  description: "Validate user helper.",

  inputs: {
    username: {
      type: "string",
      required: true,
    },
    password: {
      type: "string",
      required: true,
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs, exits) {
    // TODO
    try {
      const { username, password } = inputs;
      const user = await User.findOne({ username });
      if (!user) {
        exits.success(false);
      }
      bcrypt.compare(password, user.password, (err, res) => {
        if (err) {
          return exits.success(false);
        }
        if (res) {
          return exits.success(user);
        }
        return exits.success(false);
      });
    } catch (error) {
      exits.success(false);
    }
  },
};
