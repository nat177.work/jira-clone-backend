/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  "/": { view: "pages/homepage" },

  "POST /auth/login": { action: "Auth/login" },
  "GET /auth/logout": { action: "Auth/logout" },
  "POST /auth/register": { action: "Auth/register" },
  "GET /auth/google": { action: "Auth/google" },
  "GET /auth/google/callback": { action: "Auth/googleCallback" },
  "GET /auth/outlook": { action: "Auth/outlook" },
  "GET /auth/outlook/callback": { action: "Auth/outlookCallback" },

  "GET /projects": { action: "Projects/find" },
  "POST /projects": { action: "Projects/create" },
  "GET /projects/:id": { action: "Projects/findOne" },
  "PUT /projects/:id": { action: "Projects/update" },
  "DELETE /projects/:id": { action: "Projects/delete" },

  "GET /tasks": { action: "Tasks/find" },
  "POST /tasks": { action: "Tasks/create" },
  "GET /tasks/:id": { action: "Tasks/findOne" },
  "PUT /tasks/:id": { action: "Tasks/update" },
  "DELETE /tasks/:id": { action: "Tasks/delete" },
  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/
};
